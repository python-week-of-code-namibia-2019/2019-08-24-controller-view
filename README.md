# Python Week Of Code, Namibia 2019

[blog/urls.py](blog/urls.py) has the list of URL supported by the blog on our website.
It is producing the following error

```
  File "2019-08-24-controller-view/blog/urls.py", line 7, in <module>
    path('blog/<title>/', views.blog, name='blog')
AttributeError: module 'blog.views' has no attribute 'blog'
```

because [blog/views.py](blog/views.py) doesn' has a function called `blog`.

## Task for Instructor

1. Add

   ```
   def blog(request, title):
       return render(
           request,
           'blog/index.html',
           {}
       )
   ```

   to [blog/views.py](blog/views.py).
2. Open http://localhost:8000/blog/welcome/ in the web browser.

## Tasks for Learners

1. Edit [blog/views.py](blog/views.py) to retrieve the blog post.
   Have a look at [field lookups](https://docs.djangoproject.com/en/2.2/topics/db/queries/#field-lookups) for help.
2. Create a new template [templates/post.html](templates/post.html)
   and edit the arguments passed to the function `render`.

   Remember to use `{{ post.text|linebreaks }}`.
3. Edit the function `index` to include a list of the blog posts.